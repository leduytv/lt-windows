﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageVehicle
{
    internal class Car : Vehicle
    {
        int seat;
        String isBusinessCar;

        public int Seat { get => seat; set => seat = value; }
        public string IsBusinessCar { get => isBusinessCar; set => isBusinessCar = value; }

        public override double getRegisterFee()
        {
            if(seat <= 10)
            {
                return 240000;
            }
            else
            {
                return 320000;
            }
        }

        public override int getRegisterTime()
        {
            if(DateTime.Now.Year - dateOfManufacture.Year <= 7)
            {
                if(seat <= 9)
                {
                    if(isBusinessCar.ToLower() == "khong")
                    {
                        return 24;
                    }
                    else
                    {
                        return 12;
                    }
                }
                else
                {
                    return 12;
                }
            }
            else
            {
                return 6;
            }
        }

        public override double getTotalRegisterFee()
        {
            int totalTimes;
            int years = DateTime.Now.Year - dateOfManufacture.Year;
            if (years <= 7)
            {
                if (seat <= 9)
                {
                    if (isBusinessCar.ToLower() == "khong")
                    {
                        totalTimes = years * 12 / 24;
                    }
                    else
                    {
                        totalTimes = years;
                    }
                }
                else
                {
                    totalTimes = years;
                }
            }
            else
            {
                if(isBusinessCar.ToLower() == "khong")
                {
                    totalTimes = 3 + (years - 7);
                }
                else
                {
                    totalTimes = 7 + (years -7);
                }
            }
            return totalTimes * getRegisterFee();
        }

        public override void input()
        {
            base.input();
            Console.WriteLine("Nhap so cho ngoi: ");
            seat = int.Parse(Console.ReadLine());
            Console.WriteLine("Xe co dang ky kinh doanh khong?(Co/Khong)");
            isBusinessCar = Console.ReadLine();
        }

        public override void output() 
        {
            base.output();
            Console.WriteLine("So cho ngoi: " + seat);
            Console.WriteLine("Kinh doanh van tai: " + isBusinessCar);
        }
    }
}
