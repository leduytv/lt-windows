﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageVehicle
{
    internal abstract class Vehicle
    {
        protected DateTime dateOfManufacture;
        protected string id;

        public DateTime DateOfManufacture { get => dateOfManufacture; set => dateOfManufacture = value; }
        public string Id { get => id; set => id = value; }

        public bool isGoodId()
        {
            int count = 0;
            for (int i = 5; i < id.Length - 1; i++)
            {
                for (int j = i + 1; j < id.Length; j++)
                {
                    if (id[i] == id[j])
                    {
                        count++;
                    }
                }
            }
            if (count >= 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public abstract double getRegisterFee();

        public abstract int getRegisterTime();

        public abstract double getTotalRegisterFee();

        public virtual void input() 
        {
            Console.WriteLine("Nhap ngay san xuat: ");
            dateOfManufacture = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Nhap bien so xe: ");
            id = Console.ReadLine();
        } 

        public virtual void output()
        {
            Console.WriteLine("Ngay san xuat: {0}/{1}/{2} ", dateOfManufacture.Day, dateOfManufacture.Month, dateOfManufacture.Year);
            Console.WriteLine("Bien kiem soat: " + id);
        }
    }

}
