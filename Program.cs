﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageVehicle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List <Vehicle> list = new List<Vehicle>();
            List <Car> cars = new List<Car>();
            List <Truck> trucks = new List<Truck>();
            int selection;
            Vehicle temp;
            do
            {
                Console.WriteLine("Nhap lua chon: ");
                selection = int.Parse(Console.ReadLine());
                switch (selection)
                {
                    case 1:
                        temp = new Car();
                        temp.input();
                        list.Add(temp);
                        cars.Add((Car)temp);
                        break;
                    case 2:
                        temp = new Truck();
                        temp.input();
                        list.Add(temp);
                        trucks.Add((Truck)temp);
                        break;
                    case 3:
                        list.ForEach(x => x.output());
                        break;
                    case 4:
                        var maxSeat = cars.Max(x => x.Seat);
                        var resultCase4 = cars.FindAll(x => x.Seat == maxSeat).ToList();
                        resultCase4.ForEach(x => x.output());
                        break;
                    case 5:
                        var maxLoad = trucks.Max(x => x.TruckLoad);
                        var resultCase5 = trucks.FindAll(x => x.TruckLoad == maxLoad).ToList();
                        resultCase5.ForEach(x => x.output());
                        break;
                    case 6: 
                        var resultCase6 = list.FindAll(x => x.isGoodId() == true).ToList();
                        resultCase6.ForEach(x => x.output());
                        break;
                    case 7:
                        list.ForEach(x =>{
                            x.output();
                            Console.WriteLine("So tien dang kiem dinh ki: {0} ", x.getRegisterFee());
                        });
                        break;
                    case 8:
                        list.ForEach(x =>
                        {
                            x.output();
                            Console.WriteLine("Thoi gian dang kiem dinh ki {0} thang", x.getRegisterTime());
                        });
                        break;
                    case 9:
                        list.ForEach(x =>
                        {
                            x.output();
                            Console.WriteLine("Tong so tien da dang kiem: {0} ", x.getTotalRegisterFee());
                        });
                        break;
                    default:
                        selection = 0;
                        break;
                }
            }while (selection != 0);
            Console.ReadKey();
        }
    }
}
