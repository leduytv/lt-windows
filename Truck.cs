﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageVehicle
{
    internal class Truck : Vehicle
    {
        int truckLoad;

        public int TruckLoad { get => truckLoad; set => truckLoad = value; }

        public override double getRegisterFee()
        {
            if(truckLoad < 7)
            {
                return 320000;
            }
            else if(truckLoad >= 7 && truckLoad <= 20) 
            {
                return 350000;
            }
            else 
            {
                return 560000;
            }
        }

        public override int getRegisterTime()
        {
            if (DateTime.Now.Year - dateOfManufacture.Year <= 20)
            {
                return 6;
            }
            else
            {
                return 3;
            }
        }

        public override double getTotalRegisterFee()
        {
            int totalTimes;
            int years = DateTime.Now.Year - dateOfManufacture.Year;
            if ( years <= 20)
            {
                totalTimes = years * 12 / 6;
            }
            else
            {
                totalTimes = 40 + (years - 20) * 12 / 6;
            }
            return totalTimes * getRegisterFee();
        }

        public override void input()
        {
            base.input();
            Console.WriteLine("Nhap tai trong xe: ");
            truckLoad = int.Parse(Console.ReadLine());
        }

        public override void output() 
        {
            base.output();
            Console.WriteLine("Tai trong: " + truckLoad);
        }

    }

}
